
// @ts-ignore
const { range } = rxjs;
// @ts-ignore
const { map, filter } = rxjs.operators;

range(1, 200).pipe(
  filter(x => x % 2 === 1),
  map(x => x + x)
).subscribe(x => console.log(x));